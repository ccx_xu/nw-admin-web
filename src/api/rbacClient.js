import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/rbacClient',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/rbacClient/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/rbacClient',
    method: 'put',
    data
  })
}

export function sync() {
  return request({
    url: 'api/rbacClient/sync',
    method: 'post'
  })
}

export default { add, edit, del, sync }
